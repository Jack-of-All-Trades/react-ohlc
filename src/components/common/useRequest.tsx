import * as React from 'react';
import * as _ from 'lodash';
import Axios from 'axios';

export const getData = (resource: string) => {
    const [response, setResponse] = React.useState([]);

    const getResource = async (resource: string) => {
        const promise = await Axios.get(resource);
        setResponse(promise.data);
    }

    React.useEffect(
        () => {
            getResource(resource)
        },
        [resource]
    );

    return response;
}