import * as React from 'react';
import * as _ from 'lodash';
import { getData } from '../common/useRequest';
import { SymbolData, ChartData } from '../../services/request';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import{ Chart } from '../chart';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Header } from '../header';
import { Footer } from '../footer';

const drawerWidth: number = 240;

const styles = (theme: any) => ({
    root: {
      display: 'flex',
    },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    appBar: {
      marginLeft: drawerWidth,
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
      },
    },
    menuButton: {
      marginRight: 20,
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
    footer: {
        marginTop: 20,
        marginRight: 20
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
      width: drawerWidth,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing.unit * 3,
    },
    progress: {
      margin: theme.spacing.unit * 2,
    }
  });
  

export interface Props extends WithStyles<typeof styles> {
    classes: any;
    theme: any;
    company: string;
    symbol: string;
}

export const DrawerComponentInner = (props:Props) => {
    const [Url, setUrl] = React.useState(props.symbol);
    const [mobileOpen, setmobileOpen] = React.useState(false);
    const [companyName, setcompanyName] = React.useState(props.company);
    const {classes, theme} = props;
    const Data = getData(SymbolData);
    const items: Array<any> = _.take(Data, 10); // get first 10 items from the list

    const onSymbolSubmit = (event: any): any => {

        //get symbol and set url
        const SymbolUrl = ChartData(event.Symbol);
        setcompanyName(event["Company Name"]);
        setUrl(SymbolUrl);

        //set local-storage
        localStorage.setItem('companyname', event["Company Name"]);
        localStorage.setItem('symbol', SymbolUrl);
    }
    const handleDrawerToggle = () => {
        setmobileOpen(!mobileOpen)
    };

    const drawer = (
        <div>
          <Header />
          {items.map((elm, index) => 
        <List component="nav" className={classes.root} key={index}>
        <ListItem
            button 
            divider
            onClick={()=>onSymbolSubmit(elm)}
        >
            <ListItemText primary={elm["Company Name"]} />
        </ListItem>
        <Divider />
        </List>  
        )}
        <div className={classes.footer} />
        <Footer />
        </div>
      );
      
    return (
    <>
        <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap>
                {companyName}
            </Typography>
          </Toolbar>
        </AppBar>
        <nav className={classes.drawer}>
          <Hidden smUp implementation="css">
            <Drawer
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              open={mobileOpen}
              onClose={handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper,
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper,
              }}
              variant="permanent"
              open
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {companyName == null && <Typography paragraph component={'span'} variant={'title'}>
            Please select a company from the sidebar to view ohlc data
          </Typography>}
          {companyName != null && <Chart CompanyUrl={Url} />}
        </main>
      </div>
    </>
    );
}

export const AppDrawer = withStyles(styles, { withTheme: true })(
    DrawerComponentInner
)
  