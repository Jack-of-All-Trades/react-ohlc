import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = {
    root: {
        flexGrow: 1,
        background: "#3f51b5"
    }
  };

export interface Props extends WithStyles<typeof styles> {
    classes: any;
}

const HeaderComponentInner = (props: Props) => {
const {classes} = props;
    return (
        <div className={classes.root}>
        <AppBar position="static" color="inherit">
          <Toolbar>
           <img src="https://static1.squarespace.com/static/581ad37220099e33f332ab43/t/5be4e095352f530e5c7919d2/1551948516277/?format=100w" />
          </Toolbar>
        </AppBar>
      </div>
    );
}

export const Header = withStyles(styles)(
    HeaderComponentInner
)


