import * as React from 'react';
import CanvasJSReact from './canvasjs.react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles, WithStyles } from '@material-ui/core/styles';

let CanvasJSChart = CanvasJSReact.CanvasJSChart;

const styles = (theme: any) => ({
    progress: {
      margin: '25% 0 0 45%',
    }
  });

interface Props  extends WithStyles<typeof styles>{
    classes: any;
    CompanyUrl: string;
}
export const ChartComponentInner =  (props:Props) => {
    const [ohlcData, setohlcData] = React.useState([]);
    const [completed, setCompleted] = React.useState(false);
    const {classes} = props;
    React.useEffect(
        () => {
            (async() => {
                await fetch(props.CompanyUrl)
                .then(response => response.json())
                .then((data) => { 
                  // construct data for ohlc chart
                  const real_data = data['Time Series (Daily)'];
                  const newData = Object.keys(real_data).reduce((result, currentKey) => {
                    const value = real_data[currentKey]
                    const tmp = {
                      x: new Date(currentKey),
                      y: [parseFloat(value['1. open']), parseFloat(value['2. high']), parseFloat(value['3. low']), parseFloat(value['4. close'])],
                      color: parseFloat(value['1. open']) > parseFloat(value['4. close']) ? 'red' : 'green'
                    }
                    result.push(tmp);
                    return result;
                  }, [])
                  setohlcData(newData.slice(0, 30))
                  setCompleted(true);
                })
            })()
        },
        [props.CompanyUrl]
    );

    const options = {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light2",
        axisX: {
            interval:1,
            intervalType: "month",
            valueFormatString: "MMM"
        },
        axisY: {
            includeZero:false,
            prefix: "$",
        },
        data: [{
            type: "ohlc",
            yValueFormatString: "$###0.00",
            xValueFormatString: "YY/MM/DD",
            dataPoints: ohlcData
        }]
    }

    return (
        <>  
            {!completed  && <CircularProgress className={classes.progress} color="secondary" />}
            {completed && <CanvasJSChart options = {options}
            onRef={ref => this.chart = ref}
            />
            }
        </>
    );
}

export const Chart = withStyles(styles, { withTheme: true })(
    ChartComponentInner
)