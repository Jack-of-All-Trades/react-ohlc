const config = require('../config');

export const ChartData = (symbol) => {
    const result = `${config.api.url}&symbol=${symbol}&apikey=${config.api.key}`
    return result;
}

export const SymbolData = 'https://pkgstore.datahub.io/core/nasdaq-listings/nasdaq-listed-symbols_json/data/5c10087ff8d283899b99f1c126361fa7/nasdaq-listed-symbols_json.json'
