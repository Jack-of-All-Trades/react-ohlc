import * as React from 'react';
import {AppDrawer} from './components/drawer';

//get local storage items
const CompanyName = localStorage.getItem("companyname");
const CompanySymbol = localStorage.getItem('symbol');

export const App = () => {
    return (
        <>
            <AppDrawer company={CompanyName} symbol={CompanySymbol}/> 
        </>
    )
}